#include "TestClass.h"

TestClass::TestClass()
{

}
TestClass::~TestClass()
{

}

LM_RESULT TestClass::SetDataBy(const StPairKey& stPairKey, int nData)
{
	LM_RESULT lmRet = AddData(stPairKey, nData);

	return lmRet;
}

LM_RESULT TestClass::GetDataBy(const StPairKey& stPairKey, int& nData)
{
	LM_RESULT lmRet = GetDataByKey(stPairKey, nData);

	return lmRet;
}