#pragma once

//#include "ListManager.h"
//using namespace listmanager::impl_vec;
#include "ListManagerAsMap.h"
using namespace listmanager::impl_map;
using namespace listmanager::custom_key;

class TestClass : public ListManager<StPairKey, int>
{
public:
	TestClass();
	~TestClass();

public:
	LM_RESULT SetDataBy(const StPairKey& stPairKey, int nData);
	LM_RESULT GetDataBy(const StPairKey& stPairKey, int& nData);

};

