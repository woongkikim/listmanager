
#include "TestClass.h"

int main(void)
{
	TestClass testClass;

	int nInputData = 1;
	StPairKey stInputPairKey = StPairKey(1, "1");

	testClass.SetDataBy(stInputPairKey, nInputData);


	int nOutputData = 0;
	//StPairKey stOutputputPairKey = StPairKey(1, NULL_STR_KEY);
	StPairKey stOutputputPairKey = StPairKey(NULL_INT_KEY, "2");

	testClass.GetDataBy(stOutputputPairKey, nOutputData);

	return 0;
}